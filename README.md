# Crypto bot

### Install dependencies

```bash
pip install --no-cache-dir -r requirements.txt
```

### Usage

```bash
./main.py
```

You can set particular symbol pair by using an argument
```bash
./main.py BTCUSD
```

You can override any env parameter like so
```bash
MODE=live ./main.py BTCUSD
```

### Available modes

- "trade"
- "live"
